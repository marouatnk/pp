

<h1><center>SCRUM</center></h1>
![Related image](https://perfectial.com/wp-content/uploads/2015/06/SCRUM-01-2.jpg)

*Scrum est la **méthodologie** **la plus utilisée parmi les** [**méthodes Agiles**](https://www.planzone.fr/blog/quest-ce-que-la-methodologie-agile) **existantes**. Le terme Scrum (qui signifie mêlée) apparaît pour la première fois en 1986 dans une publication de Hirotaka Takeuchi et Ikujiro Nonaka qui décrit une nouvelle approche plus rapide et flexible pour le développement de nouveaux produits. Ils comparent alors cette nouvelle méthode au rugby à XV, le principe de base étant que l'équipe avance ensemble et soit toujours prête à réorienter le projet au fur-et-à-mesure de sa progression, tel un ballon de rugby qui doit passer de main en main jusqu'à marquer un essai.*

## **Principe**

Evidemment, l'[approche Scrum](http://www.scrumguides.org/docs/scrumguide/v1/Scrum-Guide-FR.pdf) suit les principes de la méthodologie Agile, c'est-à-dire l'implication et la participation active du client tout au long du projet.

Considéré comme un cadre (*framework* en anglais) de gestion de projet, Scrum se compose de plusieurs éléments fondamentaux :

- des **rôles**,
- des **événements**,
- des **artefacts**,
- des **règles**.



<h1>Scrum Master</h1>

![Image result for scrum master](https://evolution4all.com/content/uploads/2019/06/external-Scrum-Master_evolution4all.png)

 Enfin, **le Scrum Master** (ou SM), qu’il ne faut` 
`absolument pas confondre avec un chef de projet, a pour rôle d’animer et`
`de faciliter le travail de l’équipe de développement. Il n’est donc pas`
`là pour commander et contrôler le déroulement du projet. Il doit faire` 
`en sorte que l’équipe de développement soit pleinement opérationnelle et`
`puisse travailler dans les meilleures conditions pour la réalisation` 
`des tâches priorisées pour le sprint courant. Il doit donc la protéger` 
`de toutes les perturbations pouvant provenir de l’extérieur, mais` 
`également faciliter la communication en interne ainsi que le dialogue` 
`avec le product owner. Le Scrum Master va également s’acquitter de` 
`toutes les tâches administratives et s’assurer que la méthodologie Scrum`
`est correctement appliquée. Au sein de l’équipage du navire, le Scrum` 
`Master est donc le Bosco. Il s’assure que tout va bien sur le bateau,` 
`que l’équipage peut travailler correctement, aide chacun à bien` 
`connaître son rôle et à trouver sa place.`` 





<h1>
    Product Owner



![Image result for product Owner scrum](https://waydev.co/wp-content/uploads/2018/11/product-owner-waydev.png)



<p font-size="14">
    Le product owner – ou PO - est responsable de la définition et de la conception d'un produit. Il est chargé de mener à terme un projet en utilisant la méthode scrum*. Aussi appelé chef de projet digital, il est organisé et très rigoureux.
</p>





<h2>Fonctions</h2>
<p> Les fonctions du **product owner** comprennent, entre autres, l'analyse de toutes les exigences - contraintes et priorités - induites dans la réalisation et la conception d'un produit.
<p>Il rédige et détaille les fonctionnalités, qu'il a préalablement sélectionnées, qui apporteront le maximum de bénéfices aux futurs utilisateurs.</p>
<p>Il planifie toutes les versions possibles du produit en décrivant précisément les critères retenus pour la meilleure utilisation possible. Ces notes sont appelées *user stories*.</p>
<p>Il procède à un découpage de l'ensemble du processus en petits parcours de quelques semaines. Chaque étape est ainsi réalisée et validée au fur et à mesure afin d'avoir une vision d'ensemble de ce qui a été accompli et de ce qui reste à faire. Cette méthode permet au PO d'avoir à la fois une meilleure gestion globale, de s'adapter aux nouvelles demandes du client, mais surtout de motiver les salariés dont il a la responsabilité. </p>
<center></center><h2>Qualités</h2></center>
<p>Le **product owner** est quelqu'un de rigoureux et d'organisé. Méthodique, il a le souci du détail et un œil avisé.</p>
<p>Il est réactif, résistant au stress, mais aussi très diplomate et excellent pédagogue. Il sait motiver ses troupes et créer une bonne ambiance de travail.</p>
<p>Très bon gestionnaire, il sait s'adapter au budget alloué et aux contraintes du marché.</p>
<p>Toujours attentif aux tendances du marché, il montre des talents pour le marketing stratégique et possède de grandes connaissances techniques.</p>
<h2>Diplômes nécessaires</h2>
<p>Pour devenir product owner, il faut être issu d'une école d'ingénieur ou d'une école de commerce.Une formation dite ''alternative'' de quatre mois existe et mène à la Certification PO.</p>
<p>Les recruteurs à la recherche de PO ne manquent pas : e-commerçant, éditeur de logiciels, annonceur, agence digitale, développeur informatique, etc. </p>
<p>Toutefois, une expérience de deux ou trois ans est souvent exigée pour obtenir ce poste directement. Une évolution professionnelle peut l'amener à un poste de programme master </p>








**Authors**

   Maroua

   Frédéric